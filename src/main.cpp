#include "TcpCommunication.h"
#include "Bioveg.h"

using namespace Bioveg;

///C++ doing C++ things
const std::string Parameters::ssid = "BIOVEG_basestation";
const std::string Parameters::pass = "h6bioveg";
const std::string Parameters::ip = "10.10.10.103";
const std::string Parameters::gateway = "10.10.10.1";
const std::string Parameters::basestationIP = "10.10.10.1";
// convert float to string
Relay relay(Parameters::relayPinW);
TcpCommunication com = TcpCommunication(relay);

void setup() {
  pinMode(Parameters::relayPinW, OUTPUT);
  while (com.WifiSetup() != 1)
  {
    ;
  }
  com.startServer();
}

void loop() {
  //empty loop, since startserver() creates it's own while(true) loop
}

